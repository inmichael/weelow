# Weelow copyright, all rights reserved

from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings
from django.contrib.auth.views import login
from django.contrib.auth.decorators import login_required
from django.views.generic import TemplateView
from django.contrib.auth.decorators import login_required

from django.http import HttpResponse 
from django.views import generic
from weelowapp import views, views_login, views_features
from weelowapp.multiurl import multiurl
from weelowapp.sitemaps import StaticViewSitemap

#sitemaps
sitemaps = {
    #'todos': TodoSitemap()
    'static': StaticViewSitemap
}

handler400 = views_features.custom_400
handler403 = views_features.custom_403
handler404 = views_features.custom_404
handler500 = views_features.custom_500

urlpatterns = patterns('',

    # General
    url(r'^i/EQ3vj5aPnOw6MYDpZySKsw4PKioEqUVJ7JH6d7KMHSisEyJGwr/', include(admin.site.urls)),

    # Register
    url(r'^i/register/$', views_login.register,name='register'),

    # Index (all billboards)
    #multiurl(
    #url(r'^$',views_login.register,name='register'),
    url(r'^$',views.BillboardView.as_view(), name='billboards'),
    #),

        # Create a Billboard
#        url(r'^i/sendbillboard/$', views.SendBillboradView.as_view(), name='send-billboard'),

    # Inside Billboard
    # Order by Helium
    multiurl(
        url(r'^(?P<slug>[-\w]+)/$', views.IndexView.as_view(ordering='-helium'), name='index'),    
        url(r'^(?P<slug>[-\w]+)/$', views.UnknownIndexView.as_view(ordering='-helium'), name='index'),
        ),

    # Order by Popping time
    multiurl(
        url(r'^(?P<slug>[-\w]+)/now$', views.IndexView.as_view(ordering='pop_date'), name='index_now'),    
        url(r'^(?P<slug>[-\w]+)/now$', views.UnknownIndexView.as_view(ordering='pop_date'), name='index_now'),
        ),

        # Publish a post
        url(r'^(?P<slug>[-\w]+)/send/$', views.SendView.as_view(), name='post-send'),
      
    # Login
    url(r'^i/login/$',views_login.login,{'template_name': 'login/login.html'},name='login'),

        # Password reset
        url(r'^i/password_reset/$', views_login.password_reset, name='password_reset'),
        url(r'^i/password_reset/done/$', views_login.password_reset_done, name='password_reset_done'),
        url(r'^i/reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
            views_login.password_reset_confirm,
            name='password_reset_confirm'),
        url(r'^reset/done/$', views_login.password_reset_complete,name='password_reset_complete'),
        url(r'^i/confirm/(?P<activation_key>\w+)/(?P<username>\w+)/', views_login.register_confirm,name='register_confirmation'),
    

    # Media and static files

    url(r'^media/(?P<path>.*)$', 'django.views.static.serve', { 'document_root': settings.MEDIA_ROOT, }),
    url(r'^static/(?P<path>.*)$', 'django.views.static.serve', { 'document_root': settings.STATIC_ROOT, }),

    # Profile

    # Order by Popping time
    multiurl(
        url(r'^a/(?P<username>\w+)/$', login_required(views.ProfileView.as_view(ordering='pop_date')), name='profile'), 
        url(r'^a/(?P<username>\w+)/$', login_required(views.VisitorProfileView.as_view(ordering='pop_date')), name='profile'), 
    ),

    # Order by helium
#    multiurl(
#        url(r'^a/(?P<username>\w+)/now/$', login_required(views.ProfileView.as_view(ordering='-helium')), name='profile_now'), 
#        url(r'^a/(?P<username>\w+)/now/$', login_required(views.VisitorProfileView.as_view(ordering='-helium')), name='profile_now'), 
#    ),

        # Pick and Pop on ProfileView 
        url(r'^a/(?P<username>\w+)/create_date/$', login_required(views.Pick_Click), name='post-pick'),
        url(r'^a/(?P<username>\w+)/create_pop/$', login_required(views.Pop_Click), name='post-pop'),
        url(r'^a/(?P<username>\w+)/pop_without_helium/$', login_required(views.pop_without_helium), name='pop_without_helium'),

        # Pick and Pop on VisitorProfileView
        url(r'^a/(?P<username>\w+)/create_pick/$', views.Pick_Click, name='visitor-post-pick'), 
        url(r'^a/(?P<username>\w+)/create_pop/$', views.Pop_Click, name='visitor-post-pop'), 

        # Post on profile  
        url(r'^a/(?P<username>\w+)/send/$', views.SendView.as_view(), name='post-send-profile'),
        
        # logout  
        url(r'^a/(?P<username>\w+)/logout/$',views_login.logout_then_login,name='logout'),

        # Delete a Post
        url(r'^a/(?P<username>\w+)/(?P<pk>[\w]+)/delete/$', login_required(views.RequestDeleteView.as_view()),name='delete'),

        # Change avatar
        url(r'^a/(?P<username>\w+)/avatar/$', views_login.add, {'template_name': 'avatar_app/add.html'}, name='avatar_profile'),

    # Avatar app

    #url(r'^accounts/avatar/', include('avatar.urls')),

    # Google sitemaps

    url(r'^sitemap.xml$','django.contrib.sitemaps.views.sitemap', {'sitemaps': sitemaps,}),
    url(r'^googlec6818a23b9beef1a\.html/$', lambda r: HttpResponse("google-site-verification: googlec6818a23b9beef1a.html", content_type="text/plain")),
    url(r'^robots.txt$', include('robots.urls')),
    
)


