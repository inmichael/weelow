"""
Django settings for weelow project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '=i1twftew2c7g619u*&(7kkm-90z5gjg33pslho)oiyj9^9(ov'

# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',
    'django.contrib.sitemaps',
    'weelowapp',
    'avatar',
    'imagekit',
    'robots',
    'analytical',
)

MIDDLEWARE_CLASSES = (
    
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.common.CommonMiddleware',
)

SITE_ID = 1

# Login and registration

ACCOUNT_ACTIVATION_DAYS = 7 

REGISTRATION_AUTO_LOGIN = True

AUTHENTICATION_BACKENDS = (
    'weelowapp.backends.EmailOrUsernameModelBackend',
    'django.contrib.auth.backends.ModelBackend'
)

LOGIN_REDIRECT_URL = '/'

LOGIN_URL = '/i/login'


# Templates

TEMPLATE_DIRS = [os.path.join(BASE_DIR, 'templates')]

TEMPLATE_LOADERS = (
    'django.template.loaders.app_directories.Loader',
)

TEMPLATE_CONTEXT_PROCESSORS = (
"django.contrib.auth.context_processors.auth",
"django.core.context_processors.debug",
"django.core.context_processors.i18n",
"django.core.context_processors.media",
"django.core.context_processors.static",
"django.core.context_processors.tz",
"django.contrib.messages.context_processors.messages",
"django.core.context_processors.request",
    )

ROOT_URLCONF = 'weelow.urls'

WSGI_APPLICATION = 'weelow.wsgi.application'

AUTH_PROFILE_MODULE = 'weelow.UserProfile'

DJANGO_SETTINGS_MODULE = 'weelow.settings.common'

# Avatar settings

AVATAR_GRAVATAR_BACKUP = False
AVATAR_GRAVATAR_DEFAULT = None
AVATAR_DEFAULT_URL = 'avatar/img/default.jpg'
AVATAR_MAX_SIZE = 4024 * 4024


# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE =  'America/Los_Angeles'

USE_I18N = True

USE_L10N = False

USE_TZ = True

# Email settings

# requires to install django-smtp-ssl, https://github.com/bancek/django-smtp-ssl
# stack overflow explanation: http://stackoverflow.com/questions/17461943/sending-email-from-django-via-hosted-smtp

EMAIL_BACKEND = 'django_smtp_ssl.SSLEmailBackend'   
#EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.zoho.com'
EMAIL_HOST_USER = 'noreply@weelow.com'
EMAIL_HOST_PASSWORD = 'Weelow2014'
EMAIL_PORT = 465
DEFAULT_FROM_EMAIL = 'noreply@weelow.com'
SERVER_EMAIL = 'noreply@weelow.com'

#Analytics & metrics

#Google analytics

GOOGLE_ANALYTICS_PROPERTY_ID = 'UA-57430583-2'

#Clicky analytics

CLICKY_SITE_ID = '100835407'