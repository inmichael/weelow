from weelow.settings.common import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = ['*']


# Database 
# Postgresql 
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'weelowdemo',
        'USER': 'postgres',
        'PASSWORD': 'Weelow2014',
        'HOST': 'localhost',
        'PORT': '',
    }
}


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/


STATICFILES_FINDERS = (
  'django.contrib.staticfiles.finders.FileSystemFinder',
  'django.contrib.staticfiles.finders.AppDirectoriesFinder',
  'compressor.finders.CompressorFinder',
)



MEDIA_URL = '/media/'

STATIC_URL = '/static/'

STATIC_ROOT = '/home/michael/Desktop/repos/weelowalpha/fileupload/static/'


STATICFILES_DIRS = (
#'/home/michael/Desktop/weelowproject/weelowdev/static',
 '/users/Timur/repos/weelow/weelowdev/static/',

)



MEDIA_ROOT = '/users/Timur/repos/media/'

#MEDIA_ROOT = '/home/michael/Desktop/weelowdev/media/'



FILE_UPLOAD_HANDLERS= (
    "django.core.files.uploadhandler.MemoryFileUploadHandler",
    "django.core.files.uploadhandler.TemporaryFileUploadHandler",
)

CACHES = {

    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
    }
}

# Migration file for Dev
#MIGRATION_MODULES =  {'weelowapp': 'weelowapp.migrations.dev_mike'}
MIGRATION_MODULES =  {'weelowapp': 'weelowapp.migrations.dev_tim'}
