from django.views import generic


class custom_400(generic.TemplateView):

    template_name = "features/errors/custom_400.html"

class custom_403(generic.TemplateView):

    template_name = "features/errors/custom_403.html"

class custom_404(generic.TemplateView):

    template_name = "features/errors/custom_404.html"

class custom_500(generic.TemplateView):

    template_name = "features/errors/custom_500.html"