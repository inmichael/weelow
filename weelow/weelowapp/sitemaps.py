from django.contrib.sitemaps import Sitemap
from django.core.urlresolvers import reverse

from weelowapp.models import Billboard
"""
class TodoSitemap(Sitemap):
    changefreq = "daily"
    priority = 0.5

    def items(self):
        return Billboard.objects.all()

    def location(self,obj):
        return  r'/%s' %obj.slug
"""

class StaticViewSitemap(Sitemap):   
    changefreq = 'daily'
    priority = 0.5

    def items(self):
        return ['login', 'register',]

    def location(self, item):
        return reverse(item)