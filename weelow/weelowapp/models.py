# Weelow copyright, all rights reserved

from django.db import models
from django.db.models.signals import post_delete, post_save
from django.contrib.auth.models import User, Group
from django.utils import timezone
from django.utils.timezone import timedelta
from django.dispatch import receiver
from django.core.validators import MinValueValidator, MaxValueValidator
from django.db.models import F

from imagekit.models import ImageSpecField, ProcessedImageField
from imagekit.processors import ResizeToFill, SmartResize, Transpose

import datetime
import time

"""
This model and its receiver extend the Group table to store more information on the Groups (Billboards):
"""

class Billboard(models.Model):
    video= models.FileField(upload_to="uploads",blank=True, null=True)
    gif= models.ImageField(upload_to="uploads",blank=True, null=True)
    thumbnail_image=ProcessedImageField(
        upload_to="uploads", processors=[Transpose(), SmartResize(1200, 400)],
        format='PNG')
    helium=models.IntegerField(default=0)
    url = models.CharField(blank=True,max_length=100)
    description=models.TextField(max_length=240,blank=True, null=True)
    slug=models.SlugField(unique=True,max_length=80,blank=True, null=True)
    name=models.TextField(max_length=80,blank=False, null=True)

    def __unicode__(self):
        return self.slug

    def posts_for_user(self,user):
        #qs_pop=Pop.objects.filter(user=user).values('post')
        #qs_pick=Pick.objects.filter(user=user).values_list('post', flat=True)
        return self.posts.filter(pop_date__gte=timezone.now()).order_by('-helium','-id')[0:3]


"""
This model stores all the information on the posts: 
 - author (i.e. User table)
 - billboard (i.e. Group Profile)
 - pick, pop, number of time picked, number of time popped, helium (= number_pick-number_pop)
 - URL
 - description
"""

class Post(models.Model):
    author=models.ForeignKey(User,related_name='posts', blank=True, null=True)
    billboard=models.ForeignKey(Billboard, blank=True, null=True, related_name='posts')
    pub_date = models.DateTimeField('date published',auto_now=True)
    pop_date = models.DateTimeField('Popping time')
    thumbnail_image=ProcessedImageField(
        upload_to="uploads", processors=[Transpose(), SmartResize(500, 500)],
        format='PNG')
    description=models.TextField(max_length=240,blank=True, null=True)
    picks=models.ManyToManyField(User, through='Pick',related_name='UserPicks')
    pops=models.ManyToManyField(User, through='Pop',related_name='UserPops')
    number_pick=models.IntegerField(default=0)
    number_pop=models.IntegerField(default=0)
    helium=models.IntegerField(default=0)
    url=models.URLField(blank=True,null=True)
    url_soundcloud=models.URLField(blank=True,null=True)
    url_video=models.URLField(blank=True,null=True)
    number_days=models.IntegerField(blank=True,null=True)

    def __unicode__(self):
        return 'Post {}'.format(self.id)

def has_valid_poppingtime(self):
        now = timezone.now()
        return self.pop_date<= now

    
"""
@receiver(post_save, sender=Post)
def popping_time(*args, **kwargs):
    instance = kwargs['instance']
    pop_date_updated=instance.pop_date+datetime.timedelta(days=instance.number_days)
    Post.objects.filter(pk=instance.pk).update(pop_date=pop_date_updated)
""" 
"""
This receiver allows to delete images physically in the folder uploads when a post is deleted.
Without this receiver, the path to the folder is deleted in the Db but not the image. 
source: http://stackoverflow.com/questions/5372934/how-do-i-get-django-admin-to-delete-files-when-i-remove-an-object-
from-the-datab
"""
@receiver(post_delete, sender=Post)
def image_delete_handler(sender, **kwargs):
    photo = kwargs['instance']
    storage, name = photo.thumbnail_image.storage, photo.thumbnail_image.name
    storage.delete(name)

"""
This model and its receiver extend the user table to store more information on the user:
source: http://www.turnkeylinux.org/blog/django-profile

To call Profile attributes (i.e. activation_key, key_expires) use {{user.profile.activation_key}} in the template. 
User stands for the User table, profile for the onetoone relation between User and Profile tables called profile(i.e related_name)
and activation_key the attribute.
"""

class Profile(models.Model):
    user = models.OneToOneField(User, primary_key=True, related_name="profile")
    activation_key = models.CharField(max_length=40, blank=True)
    key_expires = models.DateTimeField(default=datetime.date.today())
      
    def __str__(self):
        return self.user.username

    class Meta:
        verbose_name_plural=u'User profiles'


@receiver(post_save, sender=User)
def create_profile_for_new_user(sender, created, instance, **kwargs):
    if created:
        profile = Profile(user=instance)
        profile.save()


"""
This adds a column slug to django's auth_group original table. The slug is then used as the id of the Group table

Group.add_to_class('slug',models.SlugField(unique=True,max_length=80,default=0, blank=True, null=True))

"""     

"""
This two models are storing information on the Picks and pops (i.e. the user_id, the post_id 
and the date of the click) 
"""

class Pick(models.Model):
    user=models.ForeignKey(User)
    post=models.ForeignKey(Post)
    pick_date = models.DateTimeField('date picked',auto_now=True)
    
class Pop(models.Model):
    user=models.ForeignKey(User)
    post=models.ForeignKey(Post)
    popped_date = models.DateTimeField('date popped',auto_now=True)

