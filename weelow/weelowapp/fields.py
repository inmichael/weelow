 # code from https://github.com/dracos/django-date-extensions
# user Dracos
# modified by Mike Friederich
# March 28th 2015

import time, re, datetime
from datetime import date

from six import with_metaclass
from django.db import models
from django import forms
from django.forms import ValidationError
from django.utils import dateformat

from .widgets import PrettyDateInput

try:
    from south.modelsinspector import add_introspection_rules
    add_introspection_rules([], ["^django_date_extensions\.fields\.ApproximateDateField"])
except ImportError:
    pass

class ApproximateDate(object):
    """A date object that accepts 0 for month or day to mean we don't
       know when it is within that month/year."""
    def __init__(self, year=0, month=0, day=0, future=False, past=False):
        if future and past:
            raise ValueError("Can't be both future and past")
        elif future or past:
            d = None
            if year or month or day:
                raise ValueError("Future or past dates can have no year, month or day")
        elif year and month and day:
            d = date(year, month, day)
        elif year and month:
            d = date(year, month, 1)
        elif year and day:
            raise ValueError("You cannot specify just a year and a day")
        elif year:
            d = date(year, 1, 1)
        else:
            raise ValueError("You must specify a year")

        self.future = future
        self.past   = past
        self.year   = year
        self.month  = month
        self.day    = day

    def __repr__(self):
        if self.future or self.past:
            return str(self)
        elif self.year and self.month and self.day:
            return "%04d-%02d-%02d" % (self.year, self.month, self.day)
        elif self.year and self.month:
            return "%04d-%02d-00" % (self.year, self.month)
        elif self.year:
            return "%04d-00-00" % self.year

    def __str__(self):
        if self.future:
            return 'future'
        if self.past:
            return 'past'
        elif self.year and self.month and self.day:
            return dateformat.format(self, "jS F Y")
        elif self.year and self.month:
            return dateformat.format(self, "F Y")
        elif self.year:
            return dateformat.format(self, "Y")

    def __eq__(self, other):
        if other is None:
            return False
        if not isinstance(other, ApproximateDate):
            return False
        elif (self.year, self.month, self.day, self.future, self.past) != (other.year, other.month, other.day, other.future, other.past):
            return False
        else:
            return True

    def __ne__(self, other):
        return not (self == other)

    def __lt__(self, other):
        if other is None:
            return False
        elif self.future or other.future:
            if self.future: 
                return False   # regardless of other.future it won't be less
            else:
                return True    # we were not in future so they are
        elif self.past or other.past:
            if other.past: 
                return False   # regardless of self.past it won't be more
            else:
                return True    # we were not in past so they are
        elif(self.year, self.month, self.day) < (other.year, other.month, other.day):
            return True
        else:
            return False

    def __le__(self, other):
        return self < other or self == other

    def __gt__(self, other):
        return not self <= other

    def __ge__(self, other):
        return self > other or self == other
    
    def __len__(self):
        return len( self.__repr__() )

ansi_date_re = re.compile(r'^\d{4}-\d{1,2}-\d{1,2}$')


# PrettyDateField - same as DateField but accepts slightly more input,
# like ApproximateDateFormField above. If initialised with future=True,
# it will assume a date without year means the current year (or the next
# year if the day is before the current date). If future=False, it does
# the same but in the past.
class PrettyDateField(forms.fields.Field):
    widget = PrettyDateInput 

    def __init__(self, future=None, *args, **kwargs):
        self.future = future
        super(PrettyDateField, self).__init__(*args, **kwargs)

    def clean(self, value):
        """
        Validates that the input can be converted to a date. Returns a Python
        datetime.date object.
        """
        super(PrettyDateField, self).clean(value)
        if value in (None, ''):
            return None
        if value == 'future':
            return ApproximateDate(future=True)
        if value == 'past':
            return ApproximateDate(past=True)
        if isinstance(value, datetime.datetime):
            return value.datetime()
        if isinstance(value, datetime.date):
            return value
        value = re.sub('(?<=\d)(st|nd|rd|th)', '', value.strip())
        for format in DATE_INPUT_FORMATS:
            try:
                return datetime.datetime.strptime(value, format)
            except ValueError:
                continue

        if self.future is None:
            raise ValidationError('Please enter a valid date.')

        # Allow year to be omitted. Do the sensible thing, either past or future.
        for format in HOUR_MINUTES_INPUT_FORMATS:
            try:
                t = time.strptime(value, format)
                month, day, hour, minute, second, yday = t[1], t[2], t[3], t[4],t[5], t[7]
                year = datetime.date.today().year
                
                if self.future and yday < int(datetime.date.today().strftime('%j')):
                    year += 1
                if not self.future and yday > int(datetime.date.today().strftime('%j')):
                    year -= 1
                return datetime.datetime(year, month, day,hour,minute,second)
            except ValueError:
                continue


        # Allow year to be omitted. Do the sensible thing, either past or future.
        for format in DAY_MONTH_INPUT_FORMATS:
            try:
                t = time.strptime(value, format)
                month, day, yday = t[1], t[2], t[7]
                year = datetime.date.today().year               
   
                if self.future and yday < int(datetime.date.today().strftime('%j')):
                    year += 1
                if not self.future and yday > int(datetime.date.today().strftime('%j')):
                    year -= 1
                return datetime.datetime(year, month, day,hour_cindarella,minute_cindarella,second_cindarella)
            except ValueError:
                continue

        raise ValidationError('Please enter a valid date.')


"""
Objective: 
    For the sake of simplicity, all the important constants used in the views are stored in this section.
    If needed, values of the constants are only changed here.

    - hour = when hour is not precised, default is 11pm (Cindarella effect)
    - minute = when minute is not precised, default is 59 minutes (Cindarella effect)
    - second = when second is not precised, default is 59 seconds (Cindarella effect)

Author: 
    Mike.

"""

hour_cindarella=23
minute_cindarella=59
second_cindarella=59


DAY_MONTH_INPUT_FORMATS = (
    '%m-%d', '%d/%m', # '10-25', '25/10'
    '%b %d', '%d %b', # 'Oct 25', '25 Oct'
    '%B %d', '%d %B', # 'October 25', '25 October'

)

HOUR_MINUTES_INPUT_FORMATS = [
    '%m-%d %I%p', '%d/%m %I%p', # '10-25', '25/10'
    '%b %d %I%p', '%d %b %I%p', # 'Oct 25', '25 Oct'
    '%B %d %I%p', '%d %B %I%p', # 'October 25', '25 October'
]


DATE_INPUT_FORMATS =  [

        # date with "-"
        '%Y-%m-%d',              # '2006-10-25'
        '%Y-%m-%d %H:%M:%S',     # '2006-10-25 14:30:59'
        '%Y-%m-%d %H:%M:%S.%f',  # '2006-10-25 14:30:59.000200'
        '%Y-%m-%d %H:%M',        # '2006-10-25 14:30'
        '%Y-%m-%d %I%p',         # '2006-10-25 2pm'
        '%Y-%m-%d %I:%M%p',      # '2006-10-25 2:30pm'

        # date with "/"
        '%m/%d/%Y',              # '10/25/2006'
        '%m/%d/%Y %H:%M:%S',     # '10/25/2006 14:30:59'
        '%m/%d/%Y %H:%M:%S.%f',  # '10/25/2006 14:30:59.000200'
        '%m/%d/%Y %H:%M',        # '10/25/2006 14:30'
        '%m/%d/%Y %I%p',         # '10-25-2006 2pm'
        '%m/%d/%Y %I:%M%p',      # '10-25-2006 2:30pm'
        
        # date with year="06" instead of "2006"
        '%m/%d/%y',              # '10/25/06'
        '%m/%d/%y %H:%M:%S',     # '10/25/06 14:30:59'
        '%m/%d/%y %H:%M:%S.%f',  # '10/25/06 14:30:59.000200'
        '%m/%d/%y %H:%M',        # '10/25/06 14:30'
        '%m/%d/%y %I%p',         # '10-25-06 2pm'
        '%m/%d/%y %I:%M%p',      # '10-25-06 2:30pm'

        # date with month as "Oct" [month before day]
        '%b %d %Y',              # 'Oct 25 2006'
        '%b %d %Y %I%p',         # 'Oct 25 2006 2pm'
        '%b %d %Y %I:%M%p',      # 'Oct 25 2006 2:30pm'

        # date with month as "Oct" and "," after month [month before day]
        '%b %d, %Y',             # 'Oct 25, 2006'
        '%b %d, %Y %I%p',        # 'Oct 25, 2006 2pm'
        '%b %d, %Y %I:%M%p',     # 'Oct 25, 2006 2:30pm'

        # date with month as "Oct" [day before month]
        '%d %b %Y',              # '25 Oct 2006'
        '%d %b %Y %I%p',         # '25 Oct 2006 2pm'
        '%d %b %Y %I:%M%p',      # '25 Oct 2006 2:30pm'

        # date with month as "October" [month before day]           
        '%B %d %Y',              # 'October 25 2006'
        '%B %d %Y %I%p',         # 'October 25 2006 2pm'
        '%B %d %Y %I:%M%p',      # 'October 25 2006 2:30pm'

        # date with month as "October" and "," after month [month before day] 
        '%B %d, %Y',             # 'October 25, 2006'
        '%B %d, %Y %I%p',        # 'October 25, 2006 2pm'
        '%B %d, %Y %I:%M%p',     # 'October 25, 2006 2:30pm'

        # date with month as "October" [day before month]           
        '%d %B %Y',              # '25 October 2006'
        '%d %B %Y %I%p',         # '25 October 2006 2pm'
        '%d %B %Y %I:%M%p',      # '25 October 2006 2:30pm'

        # date with month as "October" and "," after month [day before month] 
        '%d %B, %Y',             # '25 October, 2006'
        '%d %B, %Y %I%p',        # '25 October, 2006 2pm'
        '%d %B, %Y %I:%M%p',     # '25 October, 2006 2:30pm'

        '%A',
    ]