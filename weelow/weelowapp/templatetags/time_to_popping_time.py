# Weelow copyright, all rights reserved

from __future__ import unicode_literals

from django.template import Library, Node

register = Library()

import datetime

from django.utils.html import avoid_wrapping
from django.utils.timezone import is_aware, utc
from django.utils.translation import ugettext, ungettext_lazy

@register.filter
def timesince(d, now=None, reversed=False):
    """
    Takes two datetime objects and returns the time between d and now
    as a nicely formatted string, e.g. "10 minutes".  If d occurs after now,
    then "0 minutes" is returned.

    Units used are years, months, weeks, days, hours, and minutes.
    Seconds and microseconds are ignored.  Up to two adjacent units will be
    displayed.  For example, "2 weeks, 3 days" and "1 year, 3 months" are
    possible outputs, but "2 weeks, 3 hours" and "1 year, 5 days" are not.

    Adapted from
    http://web.archive.org/web/20060617175230/http://blog.natbat.co.uk/archive/2003/Jun/14/time_since

    Removed 18.12.2014
    (60 * 60 * 24 * 365, ungettext_lazy('%d year', '%d years')),
    (60 * 60 * 24 * 30, ungettext_lazy('%d month', '%d months')),
    (60 * 60 * 24 * 7, ungettext_lazy('%dw', '%dw')),

    Added on 28.03.2015
    - if the number of days to popping time is higher than 31 days, display
      the month of the popping time (i.e. Oct) 

    - if the number of years to popping time is higher than 1 year, display
      the year of the popping time (i.e. 2025) 


    """
    chunks = (
        (60 * 60 * 24, ungettext_lazy('%dd', '%dd')),
        (60 * 60, ungettext_lazy('%dh', '%dh')),
        (60, ungettext_lazy('< 1h', '< 1h'))
        
    )
    # Convert datetime.date to datetime.datetime for comparison.
    if not isinstance(d, datetime.datetime):
        d = datetime.datetime(d.year, d.month, d.day)
    if now and not isinstance(now, datetime.datetime):
        now = datetime.datetime(now.year, now.month, now.day)

    if not now:
        now = datetime.datetime.now(utc if is_aware(d) else None)

    delta = (d - now) if reversed else (now - d)
    # ignore microseconds
    since = delta.days * 24 * 60 * 60 + delta.seconds
    since_days = since / (24 * 60 * 60)
    since_years = since / (24 * 60 * 60 * 365)
    if since <= 0:
        # d is in the future compared to now, stop processing.
        return avoid_wrapping(ugettext('0 minutes'))
    if since_days >= 31 and since_years <1:
        # if since is more than 31 days display the month of the date d inputed.
        return d.strftime("%b")
    if since_years >= 1:
        # if since is more than 1 year display the year of the date d inputed.
        return d.strftime("%Y")
    
    for i, (seconds, name) in enumerate(chunks):
        count = since // seconds
        if count != 0:
            break

    result = avoid_wrapping(name % count)
    """
    if i + 1 < len(chunks):
        # Now get the second item
        seconds2, name2 = chunks[i + 1]
        count2 = (since - (seconds * count)) // seconds2
        if count2 != 0:
            result += ugettext(', ') + avoid_wrapping(name2 % count2)
    """
    return result

from django.utils import timezone

@register.filter
def timeuntil2(d, now=None):
    """
    Like timesince, but returns a string measuring the time until
    the given time.
    """
    now2=timezone.now()
    return timesince(d, now2, reversed=True)
