# Weelow copyright, all rights reserved

# -*- coding: utf-8 -*-

from django.shortcuts import get_object_or_404,render,redirect
from django.http import HttpResponseRedirect,HttpResponse
from django.core.urlresolvers import reverse,reverse_lazy
from django.core.paginator import Paginator 
from django.template.defaultfilters import slugify

from django.utils import timezone
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required

from django import forms
from django.views import generic
from django.db.models import F

from django.contrib.auth import authenticate, login
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.utils.http import is_safe_url

from multiurl import multiurl, ContinueResolving

from weelowapp.models import Post, Pick, Pop, Profile, Billboard
from weelowapp.forms import PostForm, BillboardForm
import json
import datetime

"""
Objective: 
	For the sake of simplicity, all the important constants used in the views are stored in this section.
	If needed, values of the constants are only changed here.

	- NumberPost_loading = number of post to display before loading new ones (infinite scroll)
	- NumberBB_loading = number of billboards to display before loading new ones (infinite scroll)

Author: 
	Mike.

"""
NumberPost_loading = 12
NumberBB_loading = 1
#NumberPost_HighHelium_loading=3

"""
Objective: 
	IndexView defines the posts presented within the billboard with following conditions:
	- popping time has not expired : pop_date>=timezone.now().
	- user has not popped the post before.
	- user has not picked the post before.
	- user can't pick or pop his own posts on the billboard.
	- posts are ordered by quantity of helium.
Django: 
	a generic listview is used here, the querry is defined inside the get_queryset.
Deleted code:
	context['is_picked'] = Pick.objects.filter(user=self.request.user).values_list('post', flat=True)

Author: 
	Mike.
"""

class IndexView(generic.ListView): 
	template_name = 'pages/billboard_feeds.html' 
	context_object_name = 'latest_question_list'
	paginate_by = NumberPost_loading
	ordering=None

	def get_context_data(self, **kwargs):
		context = super(IndexView, self).get_context_data(**kwargs)
		qs_slug=self.kwargs['slug'] 
		context['billboard'] = Billboard.objects.get(slug=qs_slug)
		context['is_mine'] = Post.objects.filter(author=self.request.user).values_list('id', flat=True)
		return context

	def get_queryset(self):
		if self.request.user.is_authenticated():
			qs_pop=Pop.objects.filter(user=self.request.user).values('post')
			qs_pick=Pick.objects.filter(user=self.request.user).values_list('post', flat=True)
			qs_slug=self.kwargs['slug'] 
			qs_billboard=Billboard.objects.get(slug=qs_slug)
			return Post.objects.filter(pop_date__gte=timezone.now(),billboard=qs_billboard).exclude(pk__in=qs_pop).exclude(pk__in=qs_pick).order_by(self.ordering,'-id')
		else:
			raise ContinueResolving


"""
Objective: 
	IndexView defines the posts presented within the billboard with following conditions:
	- popping time has not expired : pop_date>=timezone.now().
	- user has not popped the post before.
	- user has not picked the post before.
	- user can't pick or pop his own posts on the billboard.
	- posts are ordered by quantity of helium.
Django: 
	a generic listview is used here, the querry is defined inside the get_queryset.
Author: 
	Mike.
"""

class UnknownIndexView(generic.ListView): 
	template_name = 'pages/billboard_feeds.html' 
	context_object_name = 'latest_question_list'
	paginate_by = NumberPost_loading
	ordering=None

	def get_context_data(self, **kwargs):
		context = super(UnknownIndexView, self).get_context_data(**kwargs)
		qs_slug=self.kwargs['slug'] 
		#qs_group=Billboard.objects.get(slug=qs_slug)
		#context['billboard'] = Billboard.objects.get(id=qs_group)
		context['billboard'] = Billboard.objects.get(slug=qs_slug)
		return context

	def get_queryset(self):
		qs_slug=self.kwargs['slug']
		qs_billboard=Billboard.objects.get(slug=qs_slug)
		return Post.objects.filter(pop_date__gte=timezone.now(),billboard=qs_billboard).order_by(self.ordering,'-id')		

"""
Objective: 
	BillboardView displays all the billboards:
	- billboards are ordered by quantity of helium.
	- exclude the billboards created  before the Stanford
	- each billboard includes the 3 posts with the highest amount of helium excluding those that area
	already picked or popped
Django: 
	a generic TemplateView is used here, the context variable with a dictionary is added.
Author: 
	Mike.

"""
class BillboardView(generic.TemplateView): 
	template_name = 'pages/billboards.html' 
	paginate_by = NumberBB_loading

	def get_context_data(self, **kwargs):
		context = super(BillboardView, self).get_context_data(**kwargs)
		#context['billboards']=Billboard.objects.all()
		context['billboards'] = [dict(billboard=b, posts=b.posts_for_user(self.request.user)) for b in Billboard.objects.all().order_by('-helium')]
		return context

	"""
	def dispatch(self, request, *args, **kwargs):
		return redirect('/stanford-virtual-pinboard/')
	"""
	
        
"""
Objective:	
	RequestDeleteView allows you to pop (i.e. delete) your own posts in your profile page. 
	It means posts are definitely deleted from the platform and servers (different than popping 
	other people's posts).
Author: 
	Mike.
"""

class RequestDeleteView(generic.DeleteView):
    model=Post
    template_name='features/_delete_post.html'

    def get_success_url(self):
        return reverse( 'profile', kwargs = {'username': self.request.user,})          

    def get_queryset(self):
        qs = super(RequestDeleteView, self).get_queryset()
        return qs.filter(author=self.request.user)


"""
Objective:	
	ProfileView defines the posts presented in your profile page with following conditions:
	- popping time has not expired : pop_date>=timezone.now().
	- you can see you're posts or the ones you picked.
	- posts are ordered by quantity of helium.
Django: 
	multiurl (i.e. continueResolving) is called to differentiate the case 
	when another user comes to your profile page. The python package allows you to call the
	VisitorProfileView (see below) using the same url (i.e. /your_user_name/).
Author: 
	Mike.
"""

class ProfileView(generic.ListView): 
	template_name = 'pages/profile.html' 
	context_object_name = 'profile_list'
	paginate_by = NumberPost_loading
	ordering=None

	def get_context_data(self, **kwargs):
		context = super(ProfileView, self).get_context_data(**kwargs)		
		context['is_mine'] = Post.objects.filter(author=self.request.user).values_list('id', flat=True)
		context['is_picked'] = Pick.objects.filter(user=self.request.user).values_list('post', flat=True)
		return context

	def get_queryset(self):
		username = self.kwargs['username']
		if self.request.user.username==username:
			qs_user=User.objects.filter(username=username).values('id')
			qs_pick=Pick.objects.filter(user=self.request.user).values('post')
			qs_pop_without_helium=Pop.objects.filter(user=self.request.user).values('post')
			qs_gaia=Post.objects.filter(pop_date__gte=timezone.now()).filter(author=qs_user)
			q_picked=Post.objects.filter(pop_date__gte=timezone.now(), pk__in=qs_pick)
			return (qs_gaia|q_picked).exclude(pk__in=qs_pop_without_helium).order_by(self.ordering)
		else:
			raise ContinueResolving

"""
Objective:	
	VisitorProfileView defines the posts presented to another user when he sees your profile page
	with following conditions:
	- popping time has not expired : pop_date>=timezone.now().
	- you can see the posts of another user and the posts he picked.
	- posts are ordered by quantity of helium.
Django: 
	get_context_data is used here to extract the username information from the url. The template will
	call this 'username' information in /pages/visitor_profile.html.
Author: 
	Mike.
"""

class VisitorProfileView(generic.ListView): 
	template_name = 'pages/visitor_profile.html' 
	context_object_name = 'visitor_profile_list'
	paginate_by = NumberPost_loading
	ordering=None
	def get_queryset(self):
		username = self.kwargs['username']
		qs_user=User.objects.filter(username=username).values('id')
		qs_user_pick=Pick.objects.filter(user=qs_user).values('post')
		qs_pop=Pop.objects.filter(user=self.request.user).values('post')
		qs_pick=Pick.objects.filter(user=self.request.user).values('post')
		qs_mine=Post.objects.filter(author=self.request.user).values_list('id', flat=True)
		q_visitor=Post.objects.filter(pop_date__gte=timezone.now()).exclude(pk__in=qs_pop).exclude(pk__in=qs_pick).exclude(pk__in=qs_mine).filter(author=qs_user)
		q_picked=Post.objects.filter(pop_date__gte=timezone.now(), pk__in=qs_user_pick).exclude(pk__in=qs_pop).exclude(pk__in=qs_mine).exclude(pk__in=qs_pick)
		return (q_visitor|q_picked).order_by(self.ordering)

	def get_context_data(self, **kwargs):
		username = self.kwargs['username']
		context = super(VisitorProfileView, self).get_context_data(**kwargs)
		context['username'] = username
		return context

"""
Objective:	
	SendBillboradView calls the form to create a billboard:
	- Slugify's function allows to create a slug from the billboard's title.	 
Author: 
	Mike.
"""


class SendBillboradView(generic.FormView):
	form_class = BillboardForm
	template_name = 'features/_create_billboard.html'

	def get_success_url(self):
		 return reverse( 'billboards')
	
	def form_valid(self, form):
		form.save()
		return super(SendBillboradView, self).form_valid(form)

"""
Objective:	
	SendView calls the form to create a post. 
Author: 
	Mike.
Deleted code:
	def get_success_url(self):
		 return reverse( 'profile', kwargs = {'username': self.request.user,})

"""

class SendView(generic.FormView):
	form_class = PostForm
	template_name = 'features/_create_post.html'

	def get_success_url(self):
		if self.kwargs.get('slug', None):
			qs_slug=self.kwargs['slug'] 
			return reverse( 'index', kwargs = {'slug': qs_slug,})
		else:
			return reverse( 'profile', kwargs = {'username': self.request.user,})

		
	def form_valid(self, form):
		Post = form.save(commit=False)
		if  self.kwargs.get('slug', None):
			qs_slug=self.kwargs['slug']
			qs_billboard=Billboard.objects.get(slug=qs_slug)	
			Post.billboard=qs_billboard		 
		if self.request.user.is_authenticated():
			Post.author=self.request.user

		#Post.pop_date=timezone.now()+timezone.timedelta(days=2)
		
		Post.save()
		return super(SendView, self).form_valid(form)

"""
Objective:	
	Pick_click defines the backend process whent the user picks a post:
	- pick_id is retrieved from Jquery funtion "pickandpop.js".
	- user, post and date picked are saved in Pick table.
	- 'number_pick' and 'helium' variables from Post table are incremented +1.
	- 'helium' variable from Billboard table is incremented +1
Django: 
	F expressions are used to increment values.
Author: 
	Mike.
"""

def Pick_Click(request, username):
	
    if request.method == 'POST': 	
    	pick_id = request.POST.get('pickid')
    	response_data = {}

    	if not Pick.objects.filter(user=request.user,post_id=pick_id).exists():   	
	    	form=Pick(post_id=pick_id,user=request.user,pick_date=datetime.datetime.now())
	    	form.save()
	    	increment_post = Post.objects.get(id=pick_id)
	    	billboard_id=increment_post.billboard.id	
	    	increment_billboard = Billboard.objects.get(id=billboard_id)	
	    	increment_post.number_pick=F('number_pick')+1
	    	increment_post.helium=F('helium')+1
	    	increment_billboard.helium=F('helium')+1
	    	increment_post.save()
	    	increment_billboard.save()
	    	response_data['result'] = 'Congratulations! You\'re flying this ballon'

	    	return HttpResponse(
	    		json.dumps(response_data),
	    		content_type="application/json"
	    	)
    	else:
    		response_data['result'] = 'You\'ve already flown this ballon'
	    	return HttpResponse(
	    		json.dumps(response_data),
	    		content_type="application/json"
	    	)

    else:
        return HttpResponse(
            json.dumps({"nothing to see": "this isn't happening"}),
            content_type="application/json"
        )

"""
Objective:	
	Pick_pop defines the backend process whent the user pops someonelse's post:
	- pop_id is retrieved from Jquery funtion "pickandpop.js".
	- user, post and date picked are saved in Pick table.
	- 'number_pop' and 'helium' variables from Post table are respectively incremented 
	+1 and -1.
Django: 
	F expressions are used to increment values.
Author: 
	Mike
"""

def Pop_Click(request, username):
	
    if request.method == 'POST':

    	pop_id = request.POST.get('popid')
    	response_data = {}
        form=Pop(post_id=pop_id,user=request.user,popped_date=datetime.datetime.now())
        form.save()
        increment = Post.objects.get(id=pop_id)
    	increment.number_pop=F('number_pop')+1
    	increment.helium=F('helium')-1
    	increment.save()
    	response_data['result'] = 'You\'re popping this ballon'
	    		
        return HttpResponse(
            json.dumps(response_data),
            content_type="application/json"
        )
    else:
        return HttpResponse(
            json.dumps({"nothing to see": "this isn't happening"}),
            content_type="application/json"
        )

"""
Objective:	
	pop_without_helium defines the backend process when the user pops someonelse's post in his own profile page:
	- pop_id is retrieved from Jquery funtion "pickandpop.js".
	- user, post and date picked are saved in Pick table.
	- only 'number_pop' is incremented +1 but not helium
	+1 and -1.
Django: 
	F expressions are used to increment values.
Author: 
	Mike
"""

def pop_without_helium(request, username):
	
    if request.method == 'POST':

    	pop_id = request.POST.get('popid')
    	response_data = {}
        form=Pop(post_id=pop_id,user=request.user,popped_date=datetime.datetime.now())
        form.save()
        increment = Post.objects.get(id=pop_id)
        increment.number_pop=F('number_pop')+1
        increment.save()
        response_data['result'] = 'You\'re popping this ballon without helium'


        return HttpResponse(
            json.dumps(response_data),
            content_type="application/json"
        )
    else:
        return HttpResponse(
            json.dumps({"nothing to see": "this isn't happening"}),
            content_type="application/json"
        )