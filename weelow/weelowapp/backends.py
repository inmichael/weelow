from __future__ import unicode_literals
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Permission, User
from django.conf import settings

class EmailOrUsernameModelBackend(object):
    """
    This is a ModelBackend that allows authentication with either a username or an email address.

    """

    def authenticate(self, username=None, password=None):
        if '@' in username:
            try:
                user = User.objects.get(email__iexact=username)
                if user.check_password(password):
                    return user               
            except User.DoesNotExist:
                return None                   
        else:           
            try:
                user = User.objects.get(username__iexact=username)
                if user.check_password(password):
                    return user                
            except User.DoesNotExist:
                return None  

    def get_user(self, username):
        try:
            return User.objects.get(pk=username)
        except User.DoesNotExist:
            return None
    

