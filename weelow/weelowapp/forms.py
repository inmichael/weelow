# Weelow copyright, all rights reserved

from __future__ import unicode_literals

from collections import OrderedDict

from django import forms
from django.forms.utils import flatatt
from django.template import loader
from django.utils.encoding import force_bytes
from django.utils.html import format_html, format_html_join
from django.utils.http import urlsafe_base64_encode
from django.utils.safestring import mark_safe
from django.utils.text import capfirst
from django.utils.translation import ugettext, ugettext_lazy as _
from django.template.defaultfilters import slugify
import itertools

from django.contrib.auth import authenticate, get_user_model
from django.contrib.auth.models import User
from django.contrib.auth.hashers import UNUSABLE_PASSWORD_PREFIX, identify_hasher
from django.contrib.auth.tokens import default_token_generator
from django.contrib.sites.shortcuts import get_current_site
from django.utils.safestring import mark_safe

from django import forms
from django.forms.models import ModelForm
from django.utils.translation import ugettext, ugettext_lazy as _
from django.utils.text import capfirst
from django.forms.extras.widgets import SelectDateWidget

from django.contrib.auth import authenticate, get_user_model
from django.contrib.auth.models import User 

from weelowapp.models import Post, Pick, Billboard
from weelowapp.fields import PrettyDateField

import datetime

class UserCreationForm(ModelForm):
    """
    A form that creates a user, with no privileges, from the given username and
    password.
    """
    error_messages = {
        'duplicate_username': _("A user with that username already exists."),
        'password_mismatch': _("The two password fields didn't match."),
    }

    username = forms.RegexField(
        max_length=30,
        regex=r'^[\w]+$',        
        error_messages={
            'invalid': _("This value may contain only letters, numbers and "
                         "_ characters.")
                        },
        widget=forms.TextInput(attrs={'placeholder': 'Username'})
        )

    email = forms.EmailField(
        max_length=254,
        widget=forms.TextInput(attrs={'placeholder': 'Email'})
        )

    password1 = forms.CharField(
        widget=forms.PasswordInput(attrs={'placeholder': 'Password'})
        )

    class Meta:
        model = User
        fields = ("username","email")

    def clean_email(self):
        email = self.cleaned_data["email"].lower()
        try:
            User._default_manager.get(email=email)
        except User.DoesNotExist:
            return email
        raise forms.ValidationError('That email is already taken')
    
    def clean_username(self):
        # Since User.username is unique, this check is redundant,
        # but it sets a nicer error message than the ORM. See #13147.
        username = self.cleaned_data["username"]
        try:
            User._default_manager.get(username__iexact=username)
        except User.DoesNotExist:
            return username
        raise forms.ValidationError(
            self.error_messages['duplicate_username'],
            code='duplicate_username',
        )
    """   
    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(
                self.error_messages['password_mismatch'],
                code='password_mismatch',
            )
        return password2
    """


    def save(self, commit=True):
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        user.email = self.cleaned_data['email']
        if commit:
            user.is_active = False
            user.save()
        return user

class AuthenticationForm(forms.Form):
    """
    Base class for authenticating users. Extend this to get a form that accepts
    username/password logins.
    """
    username = forms.CharField(max_length=254,
        widget=forms.TextInput(attrs={'placeholder':'Email or username'}))

    password = forms.CharField(label=_("Password"),
        widget=forms.PasswordInput(attrs={'placeholder':'Password'}))

    error_messages = {
        'invalid_login': _('Please enter a correct %(username)s and password. '
                           'Note that both fields may be case-sensitive.'),
        'inactive': _("Please validate your email!"),
    }

    def __init__(self, request=None, *args, **kwargs):
        """
        The 'request' parameter is set for custom auth use by subclasses.
        The form data comes in via the standard 'data' kwarg.
        """
        self.request = request
        self.user_cache = None
        super(AuthenticationForm, self).__init__(*args, **kwargs)

        # Set the label for the "username" field.
        UserModel = get_user_model()
        self.username_field = UserModel._meta.get_field(UserModel.USERNAME_FIELD)
        if self.fields['username'].label is None:
            self.fields['username'].label = capfirst(self.username_field.verbose_name)
        
    def clean(self):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')

        if username and password:
            self.user_cache = authenticate(username=username,
                                           password=password)
            
            if self.user_cache is None:
                raise forms.ValidationError(
                    mark_safe(self.error_messages['invalid_login']),
                    code='invalid_login',
                    params={'username': self.username_field.verbose_name},
                    
                )

                          
                
            else:
                self.confirm_login_allowed(self.user_cache)

        return self.cleaned_data

    def confirm_login_allowed(self, user):
        """
        Controls whether the given User may log in. This is a policy setting,
        independent of end-user authentication. This default behavior is to
        allow login by active users, and reject login by inactive users.

        If the given user cannot log in, this method should raise a
        ``forms.ValidationError``.

        If the given user may log in, this method should return None.
        """
        
        
        if not user.is_active:
            raise forms.ValidationError(
                self.error_messages['inactive'],
                code='inactive',
            )
    

    def get_user_id(self):
        if self.user_cache:
            return self.user_cache.id
        return None

    def get_user(self):
        return self.user_cache

class PasswordResetForm(forms.Form):
    email = forms.EmailField(
        max_length=254,
        widget=forms.TextInput(attrs={'placeholder': 'Email'})
        )

    def save(self, domain_override=None,
             subject_template_name='registration/password_reset_subject.txt',
             email_template_name='registration/password_reset_email.html',
             use_https=False, token_generator=default_token_generator,
             from_email=None, request=None, html_email_template_name=None):
        """
        Generates a one-use only link for resetting password and sends to the
        user.
        """
        from django.core.mail import send_mail
        UserModel = get_user_model()
        email = self.cleaned_data["email"]
        active_users = UserModel._default_manager.filter(
            email__iexact=email, is_active=True)
        for user in active_users:
            # Make sure that no email is sent to a user that actually has
            # a password marked as unusable
            if not user.has_usable_password():
                continue
            if not domain_override:
                current_site = get_current_site(request)
                site_name = current_site.name
                domain = current_site.domain
            else:
                site_name = domain = domain_override
            c = {
                'email': user.email,
                'domain': domain,
                'site_name': site_name,
                'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                'user': user,
                'token': token_generator.make_token(user),
                'protocol': 'https' if use_https else 'http',
            }
            subject = loader.render_to_string(subject_template_name, c)
            # Email subject *must not* contain newlines
            subject = ''.join(subject.splitlines())
            email = loader.render_to_string(email_template_name, c)

            if html_email_template_name:
                html_email = loader.render_to_string(html_email_template_name, c)
            else:
                html_email = None
            send_mail(subject, email, from_email, [user.email], html_message=html_email)


class SetPasswordForm(forms.Form):
    """
    A form that lets a user change set their password without entering the old
    password
    """
    error_messages = {
        'password_mismatch': _("The two password fields didn't match."),
    }
    new_password1 = forms.CharField(
        widget=forms.PasswordInput(attrs={'placeholder': 'Password'})
        )
    new_password2 = forms.CharField(
        widget=forms.PasswordInput(attrs={'placeholder': 'Password confirmation'})
        )

    def __init__(self, user, *args, **kwargs):
        self.user = user
        super(SetPasswordForm, self).__init__(*args, **kwargs)

    def clean_new_password2(self):
        password1 = self.cleaned_data.get('new_password1')
        password2 = self.cleaned_data.get('new_password2')
        if password1 and password2:
            if password1 != password2:
                raise forms.ValidationError(
                    self.error_messages['password_mismatch'],
                    code='password_mismatch',
                )
        return password2

    def save(self, commit=True):
        self.user.set_password(self.cleaned_data['new_password1'])
        if commit:
            self.user.save()
        return self.user


class PasswordChangeForm(SetPasswordForm):
    """
    A form that lets a user change their password by entering their old
    password.
    """
    error_messages = dict(SetPasswordForm.error_messages, **{
        'password_incorrect': _("Your old password was entered incorrectly. "
                                "Please enter it again."),
    })
    old_password = forms.CharField(label=_("Old password"),
                                   widget=forms.PasswordInput)

    def clean_old_password(self):
        """
        Validates that the old_password field is correct.
        """
        old_password = self.cleaned_data["old_password"]
        if not self.user.check_password(old_password):
            raise forms.ValidationError(
                self.error_messages['password_incorrect'],
                code='password_incorrect',
            )
        return old_password

PasswordChangeForm.base_fields = OrderedDict(
    (k, PasswordChangeForm.base_fields[k])
    for k in ['old_password', 'new_password1', 'new_password2']
)

class PostForm(ModelForm): 
    """
    A form that creates a post:
    - image and title are required.
    - description can be added.        - image is automatically resized to 300*300 with imagekit package 
    (i.e. Post.thumbnail_image in models):
    https://github.com/matthewwithanm/django-imagekit
    
    Popping time:   
    'pop_date': forms.DateInput( 
            attrs={'placeholder':'Select a date'},
                    ),

    This second expression allows to display the placeholder which is not the case with the first one:

    pop_date=forms.DateField(widget=forms.DateInput(attrs={'placeholder':'Select a date'}, format='%m/%d/%Y'))

    'number_days': forms.NumberInput(  
                attrs={'min': '1', 'max': '99','placeholder': 'number of days'},

        ),   


    'description': forms.Textarea(
                attrs={'placeholder': '* Type a description | max 240 characters'}
            ),
     
    """


    def __init__(self, *args, **kwargs):
        super(PostForm, self).__init__(auto_id=True, *args, **kwargs)
        """
        self.fields['number_days'].required = True
        """

    required_css_class = "required"
    error_css_class = "error"   

    #pop_date=forms.DateTimeField(input_formats=allowed_input_formats, widget=forms.DateInput(attrs={'placeholder':'* Type a date'}))
    pop_date=PrettyDateField(required=False,future=True, widget=forms.DateInput(attrs={'placeholder':'expiry date i.e.Apr 25'}))
    class Meta:
        model=Post
        fields = ('thumbnail_image','description','url','url_soundcloud','url_video','pop_date')
        widgets = {

             'description': forms.Textarea(
                attrs={'placeholder': ' Type a description | max 240 characters'}
            ),
            
            'url': forms.TextInput(
                attrs={'placeholder': 'Add an URL'}
            ),

            'url_soundcloud': forms.TextInput(
                attrs={'placeholder': 'Add SoundClound URL'}
            ),

            'url_video': forms.TextInput(
                attrs={'placeholder': 'Add YouTube or Vimeo URL'}
            ),
        }


class BillboardForm(ModelForm):                         
    """
    A form that creates a post:
        - image and title are required.
        - description can be added.+
        - image is automatically resized to 300*300 with imagekit package 
        (i.e. Post.thumbnail_image in models):
        https://github.com/matthewwithanm/django-imagekit
        
    """

    def __init__(self, *args, **kwargs):
        super(BillboardForm, self).__init__(auto_id=True, *args, **kwargs)
        self.fields['name'].required = True

    class Meta:
        model=Billboard
        fields = ('thumbnail_image','name','description','video', 'gif')
        widgets = {
            'name': forms.Textarea(
                attrs={'placeholder': 'Type a title'}
            ),

            'description': forms.Textarea(
                attrs={'placeholder': 'Type a description'}
            ),
        }

    def save(self):
        instance = super(BillboardForm, self).save(commit=False)

        max_length = Billboard._meta.get_field('slug').max_length
        instance.slug = orig = slugify(instance.name)[:max_length]

        for x in itertools.count(1):
            if not Billboard.objects.filter(slug=instance.slug).exists():
                break

            # Truncate the original slug dynamically. Minus 1 for the hyphen.
            instance.slug = "%s-%d" % (orig[:max_length - len(str(x)) - 1], x)

        instance.save()

        return instance

    def clean_image(self):
        cleaned_data = super(PostForm,self).clean()
        image = cleaned_data.get("thumbnail_image")
                
        if image:
            if image._size > 1*1024*1024:
                raise forms.ValidationError(_(
                   'Image Must be <4mb Less'))
                return image
        else:
            raise ValidationError("Couldn't read uploaded image")
        

    
    

