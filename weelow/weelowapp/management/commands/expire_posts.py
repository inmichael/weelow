from django.core.management.base import BaseCommand
from django.db.models.signals import post_delete
from weelowapp.models import Post
from django.utils import timezone
from django.dispatch import receiver

import datetime

class Command(BaseCommand):

    help = 'Expires posts objects which are out-of-date. To run this function launch python manage.py expire_posts'

    def handle(self, *args, **options):
        print Post.objects.filter(pop_date__lt=timezone.now()).delete()

@receiver(post_delete, sender=Command)
def image_delete_handler(sender, **kwargs):
    photo = kwargs['instance']
    storage, name = photo.thumbnail_image.storage, photo.thumbnail_image.name
    storage.delete(name)