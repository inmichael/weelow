//Masonry.item-toggle-show edit


docReady(function () {

    /* Billboard Animation*/

    $(".mobile-billboard-page-box-title-btn").click(function(){
        $(".mobile-billboard-page-box-title").toggle("slide", { direction: "right"} );
      });
   
    $(".billboard-page-box-cover").on("swipeleft",function(){
        $(".mobile-billboard-page-box-title").show("slide", { direction: "right"} );
        $('i').toggleClass('glyphicon glyphicon-minus').toggleClass('glyphicon glyphicon-plus');
     });

    $(".mobile-billboard-page-box-title").on("swiperight",function(){
        $(".mobile-billboard-page-box-title").hide("slide", { direction: "right"} );
        $('i').toggleClass('glyphicon glyphicon-minus').toggleClass('glyphicon glyphicon-plus');
     });

    $('.mobile-billboard-page-box-title-btn').click( function(){
        $(this).find('i').toggleClass('glyphicon glyphicon-minus').toggleClass('glyphicon glyphicon-plus');
    });

    /* Billboard Cover resize*/


    var m1200 = window.matchMedia('all and (max-width: 1200px)');
    if(m1200.matches) {
        // the width of browser is less then 1200px
        $('.billboard-item ').height($(window).width() / 3);
    } else {
        // the width of browser is more then 1200px      
    }

    var m767 = window.matchMedia('all and (max-width: 767px)');
    if(m767.matches) {
        // the width of browser is less then 767px
        $('.billboard-page-box').height($(window).width() / 3);
    } else {
        // the width of browser is more then 767px
        $('.billboard-page-box').height($(window).width() / 3.75);
    }

 /* Billboard desable fixed background for Android device  */


  var ua = navigator.userAgent.toLowerCase();
  var isAndroid = ua.indexOf("android") > -1; //&& ua.indexOf("mobile");
  if(isAndroid) {  

     if(m767.matches) {
         $('.billboard-page-box-cover').css({
           'position': 'absolute',
           
           'width':  '100%',
           'height': '100%',
           'background-size': '100% auto',
           'background-position':'left top' 
         });
    } else {
         $('.billboard-page-box-cover').css({
           'position': 'absolute',
          
           'width':  '80%',
           'height': '100%',
           'background-size': '100% auto' ,
           'background-position':'left top'            
         });
    }
    } 

    
         
    /* Billboard enable GIF for mobile device and else desable VIDEO */

    


// Infinitescroll Container
 var container = $('.billboards-layout');

container.infinitescroll(
        {
            navSelector: '#pagination',
            nextSelector: '#pagination a',
            itemSelector: ".billboard-item",
            prefill:'true',
            debug: true, 
            loading: {
                  finishedMsg: "",
                  img: "",
                  msg: null,
                  msgText: "",
                  finished: undefined,
                  selector: null,
        }      
        },
        function (newProducts) {
           
       /* Billboard Animation*/

      $(".mobile-billboard-page-box-title-btn").click(function(){
        $(".mobile-billboard-page-box-title").toggle("slide", { direction: "right"} );
      });
   
    $(".billboard-page-box-cover").on("swipeleft",function(){
        $(".mobile-billboard-page-box-title").show("slide", { direction: "right"} );
        $('i').toggleClass('glyphicon glyphicon-resize-small').toggleClass('glyphicon glyphicon-resize-full');
     });

    $(".mobile-billboard-page-box-title").on("swiperight",function(){
        $(".mobile-billboard-page-box-title").hide("slide", { direction: "right"} );
        $('i').toggleClass('glyphicon glyphicon-resize-small').toggleClass('glyphicon glyphicon-resize-full');
     });

    $('.mobile-billboard-page-box-title-btn').click( function(){
        $(this).find('i').toggleClass('glyphicon glyphicon-resize-small').toggleClass('glyphicon glyphicon-resize-full');
    });

    /* Billboard Cover resize*/


    var m1200 = window.matchMedia('all and (max-width: 1200px)');
    if(m1200.matches) {
        // the width of browser is less then 1200px
        $('.billboard-item ').height($(window).width() / 3);
    } else {
        // the width of browser is more then 1200px      
    }

    var m767 = window.matchMedia('all and (max-width: 767px)');
    if(m767.matches) {
        // the width of browser is less then 767px
        $('.billboard-page-box').height($(window).width() / 3);
    } else {
        // the width of browser is more then 767px
        $('.billboard-page-box').height($(window).width() / 3.75);
    }

        }
    );

});


/* Billboard Animation*/

  $(document).ready(function(){



/*play when video is visible
        
      var videos = document.getElementsByTagName("video"), fraction = 1010;
      
      function checkScroll() {
      
      for(var i = 0; i < videos.length; i++) {
      
          var video = videos[i];
      
          var x = video.offsetLeft, y = video.offsetTop, w = video.offsetWidth, h = video.offsetHeight, r = x + w, //right
              b = y + h, //bottom
              visibleX, visibleY, visible;
      
              visibleX = Math.max(0, Math.min(w, window.pageXOffset + window.innerWidth - x, r - window.pageXOffset));
              visibleY = Math.max(0, Math.min(h, window.pageYOffset + window.innerHeight - y, b - window.pageYOffset));
      
              visible = visibleX * visibleY / (w * h);
      
              if (visible > fraction) {
                  video.play();
              } else {
                  video.pause();
              }
      
      }
      
      }
      
      window.addEventListener('scroll', checkScroll, false);
      window.addEventListener('resize', checkScroll, false);*/

});
