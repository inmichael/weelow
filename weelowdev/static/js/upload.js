/*! Show the uploaded image */

//set up variables
var reader = new FileReader(),
    i=0,
    numFiles = 0,
    imageFiles;

// use the FileReader to read image i
function readFile() {
    reader.readAsDataURL(imageFiles[i])
}

// define function to be run when the File
// reader has finished reading the file
reader.onloadend = function(e) {

    // make an image and append it to the div
    var image = $('<img>').attr('src', e.target.result);
    $("#images").empty();
    $(image).appendTo('#images');

    // if there are more files run the file reader again
    if (i < numFiles) {
        i++;
        readFile();
    }
};

//thumbnail_image is the id of the field thumbnail_image: {{ form.thumbnail_image.auto_id }}
$('#thumbnail_image').change(function() {
    imageFiles = document.getElementById('thumbnail_image').files
    // get the number of files
    readFile();          

});


