docReady(function () {

/* Animated Hamburger Icon*/

$('.wrapper').on('click', function(){
  
  $(this).toggleClass('checked');

})

/* Animated Navigation Menu*/

$(document).ready(function(){
    $(".nav-menu-btn").click(function(){
        $(".nav-menu").toggle("slide", { direction: "right"} );
        $(".nav-menu-outside-exit").toggle();
    });
});


$(document).ready(function(){
    $(".nav-menu").on("swiperight",function(){
        $(".nav-menu").hide("slide", { direction: "right"} );
         $('.wrapper').toggleClass('checked');
         $(".nav-menu-outside-exit").toggle();
     });
});


$(document).ready(function(){
    $(".nav-menu-outside-exit").click(function(){
        $(".nav-menu").hide("slide", { direction: "right"} );
         $('.wrapper').toggleClass('checked');
         $(".nav-menu-outside-exit").toggle();
     });
});

$(document).ready(function(){
    $(".nav-menu-enable").on("swipeleft",function(){
        $(".nav-menu").show("slide", { direction: "right"} );
         $('.wrapper').toggleClass('checked');
         $(".nav-menu-outside-exit").toggle();         
     });
});
 
 /* Animated Settings Menu*/

$(document).ready(function(){
    $(".profile-bar-settings-btn").click(function(){
        $(".profile-bar-settings-menu").toggle("slide", { direction: "up"} );
    });
});


/* Animated Settings Icon*/

$('.profile-bar-settings-btn').on('click', function(){
  
  $(this).toggleClass('checked');

})

/*Hide navbar @ scroll */

  var m767 = window.matchMedia('all and (max-width: 767px)');
  if(m767.matches) {
      // the width of browser is less then 767px
    $(document).ready(function () {
      $("div.weelow-navbar").autoHidingNavbar({
        'showOnBottom': true,
        'hideOffset' : 300
      });
    });

    /*
    $(document).scroll(function() {
      var y = $(this).scrollTop();
      if (y > 300) {
        $('.mobile-navbar-bottom').show("fade", 200);
      } else {
        $('.mobile-navbar-bottom').hide("fade", 200);
      }
    });
*/

    $(document).ready(function () {
      $("div.mobile-navbar-bottom").autoHidingBottomNavbar({
        'showOnBottom': true,
        'hideOffset' : 300
      });
    });
  } 

/*Fade header */

  $(window).scroll(function(){
    $(".title-one").css("opacity", 1 - $(window).scrollTop() / 600);
  });


});
