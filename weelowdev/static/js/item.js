//Masonry.item-toggle-show edit

docReady(function () {

 
    var m767 = window.matchMedia('all and (max-width: 767px)');
    var m500 = window.matchMedia('all and (max-width: 500px)');
    var y = $(window).scrollTop();  //your current y position on the page

    var container = $('.masonry');
    container.masonry({
        //columnWidth: ('.grid-sizer'),
        columnWidth: 240,
        //transitionDuration: 0,
        isFitWidth: true,
        gutter:10,
        itemSelector: '.item'
     });

 //Grid effect 

    //$(this).closest('.billboard-item').find(".billboard-item-info-title-bg").css({"background-image":"url('{{MEDIA_URL}}{{billboard.billboard.thumbnail_image}}')"}); 

    if(m500.matches) {

                 container.masonry({columnWidth: 100 ,gutter:0,});
                 //$(".grid-sizer" ).css({"width":"99%"}, function () {container.masonry('layout');});
                 $(".item").animate({width:'100px', height:'100px'}, 1, function () {container.masonry('layout');});
                 $(".item" ).css({"border-radius":'0px', "margin-bottom":'0px' });
                 $(".masonry" ).css({"max-width":'100%', "top":'0px'}, function () {container.masonry('layout');});
                 $(".item-inner").fadeOut(200);  
                 $(".help-on-swipe-right" ).fadeOut(100);
                 $(".item-grid-hover" ).css({"display":'block'});
                 $('.left, .right').fadeOut('left');
                 $(".pick, .pop, .pop-after-pick, .pop-delete").css({"display":'none'});


       SquareClick = 0;

          $(".grid-icon").click(function(){
            if(SquareClick==0){
                 SquareClick=1;
                 container.masonry({ columnWidth: 240, gutter:10});
                 $(".item" ).css({"border-radius":'10px', "margin-bottom":'10px'});
                 $(".masonry" ).css({"max-width":'240px', "top":'13px'});
                 $(".item-inner" ).fadeIn(1000);
                 $(".item").delay(0).animate({height:'240px',width:'240px'}, 1, function () {container.masonry('layout');});
                 $(".help-on-swipe-right" ).fadeIn(600);
                 $(".item-grid-hover" ).css({"display":'none'});
                 $('.left, .right').fadeIn();
                 $(".pick, .pop, .pop-after-pick, .pop-delete").css({"display":'block'}); 
                 

            }else{
                 SquareClick=0;
                 container.masonry({columnWidth: 100 ,gutter:0,});
                 //$(".grid-sizer" ).css({"width":"99%"}, function () {container.masonry('layout');});
                 $(".item").animate({width:'100px', height:'100px'}, 1, function () {container.masonry('layout');});
                 $(".item" ).css({"border-radius":'0px', "margin-bottom":'0px' });
                 $(".masonry" ).css({"max-width":'100%', "top":'0px'}, function () {container.masonry('layout');});
                 $(".item-inner").fadeOut(200);  
                 $(".help-on-swipe-right" ).fadeOut(100);
                 $(".item-grid-hover" ).css({"display":'block'});
                 $('.left, .right').fadeOut('left');
                 $(".pick, .pop, .pop-after-pick, .pop-delete").css({"display":'none'});

                                    
            };    
          });   

          ModalClick = 0;

          $(".item-grid-hover, .item-image").click(function(){
            if(ModalClick==0){
                 ModalClick=1;
                 $(this).parent(".item").animate({height:'300px',width:'300px'}, 250, function () {container.masonry('layout');});
                 $(this).parent(".item").css({"z-index":"11"});
                 $(this).closest('.item').find(".pick, .pop, .pop-after-pick, .pop-delete").css({"display":'block'}); 
                 $(this).closest('.item').find(".item-grid-hover").css({"display":'none'});
                 $(this).closest('.item').find(".metamorphosis-btn").css({"display":'none'});
                 $(this).closest('.item').find(".item-inner").fadeIn(200);
                 //$("html, body").animate({ scrollTop: y + "99px" }, 600);
     

            }else{
                 ModalClick=0;
                 $(this).parent(".item").animate({height:'100px',width:'100px'}, 250, function () {container.masonry('layout');});
                 $(this).parent(".item").css({"z-index":"1"});
                 $(this).closest('.item').find(".pick, .pop, .pop-after-pick, .pop-delete").css({"display":'none'}); 
                 $(this).closest('.item').find(".item-grid-hover").css({"display":'block'});
                 $(this).closest('.item').find(".metamorphosis-btn").css({"display":'block'});
                 $(this).closest('.item').find(".item-inner").fadeOut(200);
                    
            };    
          });   
     
    } else {};


 //Touch device :hover effect 

    document.addEventListener("touchstart", function() {},false);

    $('body').bind('touchstart', function() {}); 


 //PICK & POP visuel effect - remove item from page
    
    $(document).on('click', ".pick , .swipe-pick", function () {
        //msnry.remove($(this).parent(".item"));        
        container.masonry($(this).parent(".item").delay(1500).fadeOut( 400 ,function () {container.masonry('layout');
    }));
    });

    $(document).on('click', ".pop, .pop-after-pick, .swipe-pop, .swipe-pop-after-pick", function () {
        //msnry.remove($(this).parent(".item"));
        container.masonry('remove', $(this).parent(".item"));
        //msnry.layout();
        container.masonry('layout');
    });

    
    
//PICK ACTIVE effect - show content at click 

  $(document).ready(function() {
        $(".pick, .swipe-pick").each(function() {
            $(this).click(function() {
                $(this).next().fadeIn(800).show();
            });
        });
    });


//Time toggle

    $(document).ready(function() {
        $(".triangle-time", this).click(function() {
              
                $(this).closest('.item').find(".triangle-xl-time p").fadeIn( 900);
                $(this).closest('.item').find(".triangle-time a ").fadeOut(300);
                $(this).closest('.item').find(".triangle").animate({"border-width":"0 96px 96px 0"}, 400);
        });

          $(".triangle-xl-time p", this).click(function() {                          

                $(this).closest('.item').find(".triangle-xl-time p").fadeOut(300);   
                $(this).closest('.item').find(".triangle-time a").fadeIn( 900);              
                $(this).closest('.item').find(".triangle").animate({"border-width":"0 56px 56px 0"}, 500);                    
        });

    });



//Description toggle 


    if(m767.matches) {
      var post_click_element=".post-description-btn, .metamorphosis-btn"
    } else {
      var post_click_element=".post-description-btn"
    };

     DescriptionClick = 0;
     $(post_click_element).click(function(){
            if(DescriptionClick==0){
                 DescriptionClick=1;
                  $(this).closest('.item').find(".post").animate({"height": "100%","line-height": "21px"}, 600);
                  $(this).closest('.item').find(".post-url-box").fadeIn(1200);
                  $(this).closest('.item').find(".pick, .pop, .pop-after-pick, .pop-delete").css({"display": "none"});
                    
                  
            }else{
                 DescriptionClick=0;
                  $(this).closest('.item').find(".post").animate({"height": "16.67%","line-height": "40px"}, 600);   
                  $(this).closest('.item').find(".post-url-box").fadeOut(100);
                  $(this).closest('.item').find(".pick, .pop, .pop-after-pick, .pop-delete").css({"display": "table"});            

            };    
      }); 
     

// Post Widget toggle

$(document).ready(function() {
        $(".post-widget-url", this).click(function() {
                          

                $(this).closest('.item').find(".post-widget").toggle("fade", 300);                
                var iframe = $(this).closest('.item').find("#PostiFrame");
                iframe.attr("src", iframe.data("src")); 
                
                $(this).closest('.item').find(".post-widget-desable-btn, .post-url-box").toggle( "fade", 600);
                $(this).closest('.item').find(".triangle-time ").toggle( "fade", 300);
            
        });


 $(".post-widget-desable-btn", this).click(function() {            
               

                $(this).closest('.item').find(".post-widget").toggle("fade", 300);                
                var iframe = $(this).closest('.item').find("#PostiFrame");
                iframe.attr("src", iframe.data("src")); 
                
                $(this).closest('.item').find(".post-widget-desable-btn, .post-url-box").toggle( "fade", 600);
                $(this).closest('.item').find(".triangle-time ").toggle( "fade", 300);
            
        });

    });
  


//Item XL metamorphosis ----------------------
        
        if(m767.matches) {
          // the width of browser is less then 767px
         } else {

            MetamorphosisClick = 0;

          $(".metamorphosis-btn").click(function(){
            if(MetamorphosisClick==0){
                 MetamorphosisClick=1;
                 $(this).parent(".item").animate({height:'490px',width:'490px'}, 250, function () {container.masonry('layout');});
                 $(this).parent(".item").css({"z-index": "9"});
                 $(this).closest('.item').find(".pop-after-pick, .pop-delete").css({"opacity": "0.8"});
                 $(this).closest('.item').find(".pick").css({"opacity": "0.8", "right":"25%"});
                 $(this).closest('.item').find(".pop").css({"opacity": "0.8", "left":"25%"});
                 $(this).closest('.item').find(".post").css({"height": "30%","line-height": "21px" });
                 $(this).closest('.item').find(".post-description-btn").css({"display": "none"});
                 $(this).closest('.item').find(".post-url-box").fadeIn(300);     


            }else{
                 MetamorphosisClick=0;
                  $(this).parent(".item").animate({height:'240px',width:'240px'}, 200, function () {container.masonry('layout');});
                  $(this).parent(".item").css({"z-index": "0"});
                  $(this).closest('.item').find(".pop-after-pick, .pop-delete").css({"opacity": "1"});
                  $(this).closest('.item').find(".pick").css({"opacity": "1", "right":"12.5%"});
                  $(this).closest('.item').find(".pop").css({"opacity": "1", "left":"12.5%"});
                  $(this).closest('.item').find(".post").css({"height": "16.67%","line-height": "40px" });
                  $(this).closest('.item').find(".post-description-btn").css({"display": "block"});
                  $(this).closest('.item').find(".post-url-box").fadeOut(100);    
            };    
          });     
        };



//PICK & POP swipe effect and touch rule 


   if(Modernizr.touch){ 
           /*  $('.post').css({
           'display': 'block',
           'position': 'absolute',
           'width':  '100%',
           'height': '40px',
           'background-color': 'rgba(255, 255, 255, 0.8)', 
           'bottom': '0px',
           'z-index': '6',
           'line-height': '40px'
          });  
            */ 

          $(".pop").css('visibility',"hidden");
          $(".pick").css('visibility',"hidden");
          $(".pop-delete").css('visibility',"hidden");
          $(".pop-after-pick").css('visibility',"hidden");
          $(".mobile-help-box-illustration").css('display',"none");
          $(".help-box-illustration").css('display',"none");


      $(".item").on("swiperight",function(){
        if ( $(".swipe-pop", this).css('display') == 'none')
        {
        $(".swipe-pick", this).show("slide",  { direction: "left"}, 500);
        $(".item-image", this).hide("slide", { direction: "right"}, 500);
        $(".post, .post-url-box", this).css('visibility',"hidden");
        }  

        if ( $(".swipe-pick", this).css('display') == 'none')
        {
            $(".item-image", this).show("slide", { direction: "left"}, 500, function (){$(".post, .post-url-box").css('visibility',"visible"); });
        } 

        $(".swipe-pop", this).hide("slide", { direction: "right"}, 500 );

        //POP-Delete swipe effect
        if ( $(".swipe-pop-delete, .swipe-pop-after-pick", this).css('visibility') == 'visible')
        {     
        $(".swipe-pop-delete, .swipe-pop-after-pick", this).hide("slide", { direction: "right"}, 500);
        $(".item-image", this).show("slide", { direction: "left"}, 500, function (){$(".post, .post-url-box").css('visibility',"visible"); });
        }

      });   

      $(".item").on("swipeleft",function(){
        if ( $(".swipe-pick", this).css('display') == 'none')
        {
        $(".swipe-pop", this).show("slide", { direction: "right"}, 500);
        $(".item-image", this).hide("slide", { direction: "left"}, 500);
        $(".post, .post-url-box", this).css('visibility',"hidden");
        }

        if ( $(".swipe-pop", this).css('display') == 'none')
        {
         $(".item-image", this).show("slide", { direction: "right"}, 500, function (){$(".post, .post-url-box").css('visibility',"visible"); });
        }

        $(".swipe-pick", this).hide("slide", { direction: "left"}, 500);
          
        //POP-Delete swipe effect 
        if ( $(".swipe-pop-delete, .swipe-pop-after-pick", this).css('display') == 'none')
        {     
        $(".swipe-pop-delete, .swipe-pop-after-pick", this).show("slide", { direction: "right"}, 500 );
        $(".item-image", this).hide("slide", { direction: "left"}, 500);
        $(".post, .post-url-box", this).css('visibility',"hidden");
        }

      });   
    } 
    else{

      $(".pop").css('visibility',"visible");
      $(".pick").css('visibility',"visible");
      //$(".pop-delete").css('visibility',"visible");
      //$(".pop-after-pick").css('visibility',"visible");
      //$(".mobile-help-box-swipe-illustration").css('display',"none");
      //$(".help-box-swipe-illustration").css('display',"none");

    }
 

// Infinitescroll Container

container.infinitescroll(
        {
            navSelector: '#pagination',
            nextSelector: '#pagination a',
            itemSelector: ".item",
            prefill:'true',
            debug: true, 
            loading: {
                  finishedMsg: "",
                  img: "",
                  msg: null,
                  msgText: "",
                  finished: undefined,
                  selector: null,
        }      
        },
        function (newProducts) {
            var newProds = $(newProducts);
                //newProds.animate({"opacity": 1});
                container.masonry("appended", newProds, true);

            $(newProducts).find(".pop, .pop-after-pick, .swipe-pop, .swipe-pop-after-pick").click(function (){
                $.ajax({
                    type: "POST",
                    url: $(this).attr('data-done-ref'),  // or just url: "/my-url/path/"
                    data: {popid: $(this).attr('data-id')},
                    success: function(json){
                     console.log(json); 
                    },                    
                });    
            });

             $(newProducts).find(".pick, .swipe-pick").click(function (){
                $.ajax({
                      type: "POST",
                      url: $(this).attr('data-done-ref'),  // or just url: "/my-url/path/"
                      data: {pickid: $(this).attr('data-id')},
                      success: function(json){
                      console.log(json); 
                      },                     
                });
            });

           
            $(newProducts).find(".pick, .swipe-pick").each(function() {
            $(this).click(function() {
                $(this).next().fadeIn(800).show();
            });
        });  

  //Time toggle
  

    $(newProducts).find(".triangle-time", this).each(function() {
          $(this).click(function() {
              
                $(this).closest('.item').find(".triangle-xl-time p").fadeIn( 900);
                $(this).closest('.item').find(".triangle-time a ").fadeOut(300);
                $(this).closest('.item').find(".triangle").animate({"border-width":"0 96px 96px 0"}, 400);
        });
     });

     $(newProducts).find(".triangle-xl-time p", this).each(function() {
          $(this).click(function() {                          

                $(this).closest('.item').find(".triangle-xl-time p").fadeOut(300);   
                $(this).closest('.item').find(".triangle-time a").fadeIn( 900);              
                $(this).closest('.item').find(".triangle").animate({"border-width":"0 56px 56px 0"}, 500);                    
        });

    }); 
  

  //Description toogle 

       DescriptionClick = 0;
       $(newProducts).find(post_click_element).each(function() {
       $(this).click(function(){
              if(DescriptionClick==0){
                   DescriptionClick=1;
                    $(this).closest('.item').find(".post").animate({"height": "100%","line-height": "21px"}, 600);
                    $(this).closest('.item').find(".post-url-box").fadeIn(1200);
                    $(this).closest('.item').find(".pick, .pop, .pop-after-pick, .pop-delete").css({"display": "none"});
                      
                    
              }else{
                   DescriptionClick=0;
                    $(this).closest('.item').find(".post").animate({"height": "16.67%","line-height": "40px"}, 600);   
                    $(this).closest('.item').find(".post-url-box").fadeOut(100);
                    $(this).closest('.item').find(".pick, .pop, .pop-after-pick, .pop-delete").css({"display": "table"});            

              };    
            }); 
       }); 
  
    // Post Widget toggle

    $(newProducts).find(".post-widget-url", this).each(function() {
          $(this).click(function() {
                            

                  $(this).closest('.item').find(".post-widget").toggle("fade", 300);                
                  var iframe = $(this).closest('.item').find("#PostiFrame");
                  iframe.attr("src", iframe.data("src")); 
                  
                  $(this).closest('.item').find(".post-widget-desable-btn, .post-url-box").toggle( "fade", 600);
                  $(this).closest('.item').find(".triangle-time ").toggle( "fade", 300);
              
          });
    });

   $(newProducts).find(".post-widget-desable-btn", this).each(function() {
          $(this).click(function() {          
                 

                  $(this).closest('.item').find(".post-widget").toggle("fade", 300);                
                  var iframe = $(this).closest('.item').find("#PostiFrame");
                  iframe.attr("src", iframe.data("src")); 
                  
                  $(this).closest('.item').find(".post-widget-desable-btn, .post-url-box").toggle( "fade", 600);
                  $(this).closest('.item').find(".triangle-time ").toggle( "fade", 300);
              
          });
    });
     
 //Grid effect 


    if(m500.matches) {

       container.masonry({columnWidth: 100 ,gutter:0,});
                 //$(".grid-sizer" ).css({"width":"99%"}, function () {container.masonry('layout');});
                 $(".item").animate({width:'100px', height:'100px'}, 1, function () {container.masonry('layout');});
                 $(".item" ).css({"border-radius":'0px', "margin-bottom":'0px' });
                 $(".masonry" ).css({"max-width":'100%', "top":'0px'}, function () {container.masonry('layout');});
                 $(".item-inner").fadeOut(200);  
                 $(".help-on-swipe-right" ).fadeOut(100);
                 $(".item-grid-hover" ).css({"display":'block'});
                 $('.left, .right').fadeOut('left');
                 $(".pick, .pop, .pop-after-pick, .pop-delete").css({"display":'none'});

       SquareClick = 0;
          $(newProducts).find(".grid-icon").each(function() {
          $(this).click(function() {
          
               if(SquareClick==0){
                 SquareClick=1;
                 container.masonry({ columnWidth: 240, gutter:10});
                 $(".item" ).css({"border-radius":'10px', "margin-bottom":'10px'});
                 $(".masonry" ).css({"max-width":'240px', "top":'13px'});
                 $(".item-inner" ).fadeIn(1000);
                 $(".item").delay(0).animate({height:'240px',width:'240px'}, 1, function () {container.masonry('layout');});
                 $(".help-on-swipe-right" ).fadeIn(600);
                 $(".item-grid-hover" ).css({"display":'none'});
                 $('.left, .right').fadeIn();
                 $(".pick, .pop, .pop-after-pick, .pop-delete").css({"display":'block'}); 
                 

            }else{
                 SquareClick=0;
                 container.masonry({columnWidth: 100 ,gutter:0,});
                 //$(".grid-sizer" ).css({"width":"99%"}, function () {container.masonry('layout');});
                 $(".item").animate({width:'100px', height:'100px'}, 1, function () {container.masonry('layout');});
                 $(".item" ).css({"border-radius":'0px', "margin-bottom":'0px' });
                 $(".masonry" ).css({"max-width":'100%', "top":'0px'}, function () {container.masonry('layout');});
                 $(".item-inner").fadeOut(200);  
                 $(".help-on-swipe-right" ).fadeOut(100);
                 $(".item-grid-hover" ).css({"display":'block'});
                 $('.left, .right').fadeOut('left');
                 $(".pick, .pop, .pop-after-pick, .pop-delete").css({"display":'none'});

                                    
            };    
          });  
          });    
   
          ModalClick = 0;
          $(newProducts).find(".item-grid-hover, .item-image").each(function() {
          $(this).click(function() {
          
            if(ModalClick==0){
                 ModalClick=1;
                 $(this).parent(".item").animate({height:'300px',width:'300px'}, 250, function () {container.masonry('layout');});
                 $(this).parent(".item").css({"z-index":"11"});
                 $(this).closest('.item').find(".pick, .pop, .pop-after-pick, .pop-delete").css({"display":'block'}); 
                 $(this).closest('.item').find(".item-grid-hover").css({"display":'none'});
                 $(this).closest('.item').find(".metamorphosis-btn").css({"display":'none'});
                 $(this).closest('.item').find(".item-inner").fadeIn(200);
                 //$("html, body").animate({ scrollTop: y + "99px" }, 600);
     

            }else{
                 ModalClick=0;
                 $(this).parent(".item").animate({height:'100px',width:'100px'}, 250, function () {container.masonry('layout');});
                 $(this).parent(".item").css({"z-index":"1"});
                 $(this).closest('.item').find(".pick, .pop, .pop-after-pick, .pop-delete").css({"display":'none'}); 
                 $(this).closest('.item').find(".item-grid-hover").css({"display":'block'});
                 $(this).closest('.item').find(".metamorphosis-btn").css({"display":'block'});
                 $(this).closest('.item').find(".item-inner").fadeOut(200);
                    
            };    
          });
          });    
      
    } else {};
     
 //Item XL metamorphosis ----------------------

  
        
        if(m767.matches) {
          // the width of browser is less then 767px
         } else {

            MetamorphosisClick = 0;
          $(newProducts).find(".metamorphosis-btn").each(function() {
          $(this).click(function() {
          
            if(MetamorphosisClick==0){
                 MetamorphosisClick=1;
                 $(this).parent(".item").animate({height:'490px',width:'490px'}, 250, function () {container.masonry('layout');});
                 $(this).parent(".item").css({"z-index": "9"});
                 $(this).closest('.item').find(".pop-after-pick, .pop-delete").css({"opacity": "0.8"});
                 $(this).closest('.item').find(".pick").css({"opacity": "0.8", "right":"25%"});
                 $(this).closest('.item').find(".pop").css({"opacity": "0.8", "left":"25%"});
                 $(this).closest('.item').find(".post").css({"height": "30%","line-height": "21px" });
                 $(this).closest('.item').find(".post-description-btn").css({"display": "none"});
                 $(this).closest('.item').find(".post-url-box").fadeIn(300);     


            }else{
                 MetamorphosisClick=0;
                  $(this).parent(".item").animate({height:'240px',width:'240px'}, 200, function () {container.masonry('layout');});
                  $(this).parent(".item").css({"z-index": "0"});
                  $(this).closest('.item').find(".pop-after-pick, .pop-delete").css({"opacity": "1"});
                  $(this).closest('.item').find(".pick").css({"opacity": "1", "right":"12.5%"});
                  $(this).closest('.item').find(".pop").css({"opacity": "1", "left":"12.5%"});
                  $(this).closest('.item').find(".post").css({"height": "16.67%","line-height": "40px" });
                  $(this).closest('.item').find(".post-description-btn").css({"display": "block"});
                  $(this).closest('.item').find(".post-url-box").fadeOut(100);    
            };    
          });
          });      
        }



//PICK & POP swipe effect and touch rule

    if(Modernizr.touch){ 
           /*  $('.post').css({
           'display': 'block',
           'position': 'absolute',
           'width':  '100%',
           'height': '40px',
           'background-color': 'rgba(255, 255, 255, 0.8)', 
           'bottom': '0px',
           'z-index': '6',
           'line-height': '40px'
          });  
            */ 

          $(".pop").css('visibility',"hidden");
          $(".pick").css('visibility',"hidden");
          $(".pop-delete").css('visibility',"hidden");
          $(".pop-after-pick").css('visibility',"hidden");
          $(".mobile-help-box-illustration").css('display',"none");
          $(".help-box-illustration").css('display',"none");


      $(".item").on("swiperight",function(){
        if ( $(".swipe-pop", this).css('display') == 'none')
        {
        $(".swipe-pick", this).show("slide",  { direction: "left"}, 500);
        $(".item-image", this).hide("slide", { direction: "right"}, 500);
        $(".post, .post-url-box", this).css('visibility',"hidden");
        }  

        if ( $(".swipe-pick", this).css('display') == 'none')
        {
            $(".item-image", this).show("slide", { direction: "left"}, 500, function (){$(".post, .post-url-box").css('visibility',"visible"); });
        } 

        $(".swipe-pop", this).hide("slide", { direction: "right"}, 500 );

        //POP-Delete swipe effect
        if ( $(".swipe-pop-delete, .swipe-pop-after-pick", this).css('visibility') == 'visible')
        {     
        $(".swipe-pop-delete, .swipe-pop-after-pick", this).hide("slide", { direction: "right"}, 500);
        $(".item-image", this).show("slide", { direction: "left"}, 500, function (){$(".post, .post-url-box").css('visibility',"visible"); });
        }

      });   

      $(".item").on("swipeleft",function(){
        if ( $(".swipe-pick", this).css('display') == 'none')
        {
        $(".swipe-pop", this).show("slide", { direction: "right"}, 500);
        $(".item-image", this).hide("slide", { direction: "left"}, 500);
        $(".post, .post-url-box", this).css('visibility',"hidden");
        }

        if ( $(".swipe-pop", this).css('display') == 'none')
        {
         $(".item-image", this).show("slide", { direction: "right"}, 500, function (){$(".post, .post-url-box").css('visibility',"visible"); });
        }

        $(".swipe-pick", this).hide("slide", { direction: "left"}, 500);
          
        //POP-Delete swipe effect 
        if ( $(".swipe-pop-delete, .swipe-pop-after-pick", this).css('display') == 'none')
        {     
        $(".swipe-pop-delete, .swipe-pop-after-pick", this).show("slide", { direction: "right"}, 500 );
        $(".item-image", this).hide("slide", { direction: "left"}, 500);
        $(".post, .post-url-box", this).css('visibility',"hidden");
        }

      });   
    } 
    else{

      $(".pop").css('visibility',"visible");
      $(".pick").css('visibility',"visible");
      $(".mobile-help-box-swipe-illustration").css('display',"none");
      $(".help-box-swipe-illustration").css('display',"none");

    }
        

    }
  );
});
