docReady(function () {

//Loader Animation

 $(document).ready(function() {
    $(".active-loader", this).click(function() {
      $("body").css({"overflow": "hidden"});              
      $(this).closest('body').find(".loader-bg, .loader-animation.left , .loader-animation.right").fadeIn( 600
         ,function () {
           $(this).closest('body').find(".billboards-layout, .masonry, .billboard-page-box, .profile-bar, .new-post, .profile-avatar, .content-main, #container, video").css({"filter": "blur(1px)", "-webkit-filter": "blur(1px)","overflow": "hidden", "opacity": "1"});
         }
      );
      location.reload();             
   });
});


//Sort by Animation


 $(document).ready(function() {
    $(".active-sortby-loader", this).click(function() {
                 
      //$(this).closest('body').find(".sortby-ppt-btn").toggle("fade", 900);
      //$(this).closest('body').find(".sortby-helium-btn").toggle("fade", 900); 

      $("body").css({"overflow": "hidden"});              
      $(this).closest('body').find(".loader-bg-sortby").fadeIn( 600
         ,function () {
           $(this).closest('body').find(".billboards-layout, .masonry, .billboard-page-box, .profile-bar, .new-post, .profile-avatar, .content-main, #container, video").css({"filter": "blur(1px)", "-webkit-filter": "blur(1px)","overflow": "hidden", "opacity": "1"});
         }
      );    
                 
   });

    $(".sortby-ppt-btn", this).click(function() {
                 
      $(this).closest('body').find(".sortby-ppt-btn").css({"background-color": "#35bb85"});
      $(this).closest('body').find(".sortby-ppt-btn p").css({"color": "#fff"});                
                 
   });

    $(".sortby-helium-btn", this).click(function() {
                 
      $(this).closest('body').find(".sortby-helium-btn").css({"background-color": "#E9DF0E"});
      $(this).closest('body').find(".sortby-helium-btn p").css({"color": "#34495D"});                
                 
   });
});

});


